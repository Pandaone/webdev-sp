#pragma once

#include <time.h>
#include <sys/time.h>

#define MY_PATH_LEN 4096

typedef struct {

    char method[8];
    
    char * host;
    char * path;
    char * file;

    struct timeval request_time;
    struct timezone request_timezone;

} http_req_t;

http_req_t * parse_request(char *data);
void destroy_http_req(http_req_t *self);