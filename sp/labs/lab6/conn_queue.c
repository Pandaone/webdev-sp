#include <stdbool.h>
#include <stdlib.h>

#include "conn_queue.h"

conn_queue_t * conn_queue_init(void) {

    conn_queue_t *self = malloc(sizeof(conn_queue_t));
    
    self->end = NULL;
    self->start = NULL;
    
    pthread_mutex_init(&self->lock, NULL);
    sem_init(&self->is_empty_queue, false, 0);
    
    return self;
}

int conn_queue_push(conn_queue_t * self, connection_t * conn) {
    
    pthread_mutex_lock(&self->lock);
    
    if (self->start == NULL && self->end == NULL) {
        self->start = conn;
        self->end = conn;
    } 
    
    else {
        self->end->next = conn;
        self->end = conn;
    }

    conn->next = NULL;
    
    sem_post(&self->is_empty_queue);
    pthread_mutex_unlock(&self->lock);
    
    return 0;
}

connection_t * conn_queue_pull(conn_queue_t * self) {
    connection_t * res;
    
    sem_wait(&self->is_empty_queue);
    pthread_mutex_lock(&self->lock);
    
    res = self->start;
    
    self->start = self->start->next;
    if (self->start == NULL) {
        self->end = NULL;
    }
    
    pthread_mutex_unlock(&self->lock);
    return res;
}

void conn_queue_destroy(conn_queue_t * self) {

    pthread_mutex_destroy(&self->lock);
    sem_destroy(&self->is_empty_queue);
    
    connection_t * cur = self->start;
    
    while (cur != NULL && cur->next != NULL) {
        free(cur);
        cur = cur->next;
    }
}
