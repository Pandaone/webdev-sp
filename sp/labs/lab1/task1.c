#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Error! No input file\n");
        exit(1);
    }
    if (argc < 3)
    {
        printf("Error! No output file\n");
        exit(1);
    }
    char *inputFile = argv[1];
    char *outputFile = argv[2];
    int fIn = open(inputFile, O_RDONLY);
    if (fIn == -1)
    {
        printf("Problem with input file %s.\n Error: %s\n", inputFile, strerror(errno));
        exit(1);
    }
    int fOut = open(outputFile, O_CREAT | O_WRONLY | O_APPEND, 0644);
    if (fOut == -1)
    {
        printf("Problem with output file %s.\n Error: %s\n", outputFile, strerror(errno));
        exit(1);
    }
    char buffer[512];
    int bytesChangedCnt = 0;
    while (1)
    {
        int bytes = read(fIn, buffer, sizeof(buffer));
        for (int i = 0; i < sizeof(buffer); i++)
        {
            if (buffer[i] >= 'a' && buffer[i] <= 'z')
            {
                buffer[i] -= 32;
                bytesChangedCnt++;
            }
        }
        write(fOut, buffer, sizeof(buffer));
        if (bytes < sizeof(buffer))
            break;
    }
    printf("%d bytes were changed\n", bytesChangedCnt);
    close(fIn);
    close(fOut);
    return 0;
}