#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int main() {
    pid_t child = fork();
    if (child == 0) {
        printf("Child process:\n");
        printf("Child Pid: %d\n", getpid());
        printf("Child Ppid: %d\n", getppid());
        printf("Child Gid: %d\n", getgid());
        printf("Child Egid: %d\n", getegid());
        printf("Child Sid: %d\n", getsid(0));
        printf("Child Uid: %d\n", getuid());
        printf("Child Euid: %d\n", geteuid());
        printf("Stoped child process\n");
    } else {
        printf("Parent process:\n");
        printf("Parent Pid: %d\n", getpid());
        printf("Parent Ppid: %d\n", getppid());
        printf("Parent Gid: %d\n", getgid());
        printf("Parent Egid: %d\n", getegid());
        printf("Parent Sid: %d\n", getsid(0));
        printf("Parent Uid: %d\n", getuid());
        printf("Parent Euid: %d\n", geteuid());
        printf("Child process pid: %i\n", child);
        wait(NULL);
        printf("Stoped parent process\n");
    }
    return 0;
}