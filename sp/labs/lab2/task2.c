#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

int main(int argc, char* argv[])
{
    FILE *logsInput = fopen("./log.txt", "w");
    fprintf(logsInput, "Program started...\n");

    pid_t process_id = fork();
    if (process_id < 0)
    {
        printf("fork failed!\n");
        exit(EXIT_FAILURE);
    }

    if (process_id > 0)
    {
        fprintf(logsInput, "process_id of child process is %d \n", process_id);
        exit(EXIT_SUCCESS);
    }
    
    pid_t sid = setsid();
    if(sid < 0)
    {
        fprintf(stderr, "Error while set sid\n");
        exit(EXIT_FAILURE);
    }

    if (chdir("/") < 0) {
        fprintf(stderr, "Error while chdir to \"/\"\n");
        exit(EXIT_FAILURE);
    }
    
    int size = getdtablesize();
    for (int i = 0; i < size; i++)
    {
        close(i);
    }
    
    open("/dev/null", O_RDONLY, stdin);
    open("/dev/null", O_WRONLY, stdout);
    open("/dev/null", O_WRONLY, stderr);

    FILE *newLogInput = fopen("./log.txt", "a+");
    fprintf(logsInput, "Pid: %d\n", getpid());
    fprintf(logsInput, "Ppid: %d\n", getppid());
    fprintf(logsInput, "Gid: %d\n", getgid());
    fprintf(logsInput, "Egid: %d\n", getegid());
    fprintf(logsInput, "Sid: %d\n", getsid(0));
    fprintf(logsInput, "Uid: %d\n", getuid());
    fprintf(logsInput, "Euid: %d\n", geteuid());
    
    while (1)
    {
        sleep(1);
        fprintf(logsInput, "Logging info...\n");
        fflush(logsInput);
    }
    fclose(logsInput);
    exit(EXIT_SUCCESS);
}