const FCGI_URL = 'https://sandbox.ee.kpi.ua/~kp9311';

const fetchMpstat = async () => {
	return (await axios.get(FCGI_URL + '/fcgi/api/mpstat')).data;
}
const fetchIostat = async () => {
	return (await axios.get(FCGI_URL + '/fcgi/api/iostat')).data;
}
const fetchFree = async () => {
	return (await axios.get(FCGI_URL + '/fcgi/api/free')).data;
}

const fetchData = async () => {
  try {
    const data = await Promise.all([fetchMpstat(), fetchIostat(), fetchFree()]);
		return data;
  } catch (err) {
    console.error('Fetch data error:', err.message);
  }
};

const provideDataToDOM = async () => {
	const [mpstat, iostat, free] = await fetchData();

	const $mpstat = document.querySelector('.mpstat pre');
	const $iostat = document.querySelector('.iostat pre');
	const $free = document.querySelector('.free pre');

	$mpstat.textContent = mpstat;
	$iostat.textContent = iostat;
	$free.textContent = free;	
}

globalThis.setInterval(provideDataToDOM, 10000);
provideDataToDOM();
