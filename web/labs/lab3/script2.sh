#!/bin/bash 

function getHTML() {
echo "
<form method='POST' action='./script2.sh'>
    <input type='text' name='filename' value='${1}'/>
    <input type='submit' value='Upload' name='action'/>
    (ex: file.txt)
    <br/>
    <textarea rows='15' cols='100' name='content'>${2}</textarea><br>
    <input type='submit' value='Save' name='action'/>
</form>
<p>
    <b style='color: red;'>${3}</b>
</p>"
}

function urldecode() { : "${*//+/ }"; echo -e "${_//%/\\x}"; }

# Read input data
read INPUT_VARIABLES
IFS="&"
set -- $INPUT_VARIABLES
array=($@)
declare -A hash
for i in "${array[@]}"; do IFS="=" ; set -- $i; hash[$1]=$2; done

echo "Status: 200"
echo "Content-Type: text/html"
echo



cat << EOF
<!DOCTYPE html>
<html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
<body>
    $(if [ "$REQUEST_METHOD" = "GET" ]
    then         
        getHTML "" "" ""  
    elif [ $REQUEST_METHOD = "POST" ]
    then
      filename=$(urldecode ${hash["filename"]})
      fileContent=$(cat $filename) || error="File read failed!"
      
      action=${hash["action"]}
      if [ $action = "Save" ]
      then
        newContent=$(urldecode ${hash["content"]})
        newFile="./backup.txt"
        cp "./file.txt" $newFile
        echo "$newContent" > $newFile || error="Write file failed!"
      fi
      
      if [ $error ]
      then
        getHTML "$filename" "$fileContent" "ERROR: $error"
      else
        getHTML "$filename" "$fileContent" ""
      fi
    
    fi)
</body>
</html>
EOF