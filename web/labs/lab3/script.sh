#!/bin/bash
HTML="
<p>
    Поточна дата та час: $(date) <br>
    Час від запуску сервера: $(uptime) <br>
    Операційна система сервера: $(uname) <br>
    Інформацію про користувача: $(id) <br>
    Публічна адреса клієнта: $HTTP_X_FORWARDED_FOR <br>
    Інформація про браузер: $HTTP_USER_AGENT <br>
</p>
<form action='./script.sh' method='post'>
    <input type='text' name='command' />
    <button>Submit</button>
</form>
"

cat <<EOF
Content-type: text/html

<!DOCTYPE html>
<html>
  <head>
      <meta http-equiv="Content-Type" content="charset=UTF-8">
  </head>
<body>      
    $(echo ${HTML} 
    if [ "$REQUEST_METHOD" = "POST" ]
    then
        if [ "$CONTENT_LENGTH" -gt 0 ]
        then    
            read -n $CONTENT_LENGTH POST_DATA < $(/dev/stdin)
            POST_DATA=$(cat)
            command=(${POST_DATA//=/ })
            if [ "${command[0]}" == "command" ]
            then
                commandInput=$(urldecode ${command[1]})
                commandResult=$($commandInput)
                $(eval $commandInput): ${commandResult:= unable to execute}
                echo "<hr/><p style='color: red;'>Input command: '$commandInput' </br>Output command: $commandResult</p>"
            fi
        fi
        
    fi)
</body>
</html>
EOF